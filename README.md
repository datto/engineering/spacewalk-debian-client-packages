# Spacewalk client packages for Debian/Ubuntu systems (DEAD)

This repository contains the packaging for the Spacewalk client software for Debian/Ubuntu systems.

The packaging is forked from [the Spacewalk project](https://spacewalkproject.github.io/), and remains licensed under the same terms (GNU GPLv2+).

This project held the packaging for Spacewalk client packaging for Debian/Ubuntu prior to it being upstreamed into the main Spacewalk repository.

Thus, this project is officially dead, and we encourage people to look at [the Spacewalk repo on GitHub for the latest code](https://github.com/spacewalkproject/spacewalk)).
