#!/bin/bash
# Script to install and setup debbuild
# Author: Neal Gompa <ngompa@datto.com>
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later

# Copyright (C) 2018  Datto, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


set -eu -o pipefail

# Install dependencies for debbuild
DEBIAN_FRONTEND=noninteractive apt-get -y install lsb-release xz-utils perl sed dpkg-dev pax bzip2 fakeroot patch wget

# Download and install debbuild
wget https://github.com/ascherer/debbuild/releases/download/18.6.1/debbuild_18.6.1-ascherer.ubuntu16.04_all.deb
dpkg -i debbuild_18.6.1-ascherer.ubuntu16.04_all.deb

# Configure debbuild behavior for CI with ~/.debmacros
echo "%_default_verbosity 2" >> ~/.debmacros
echo "%_topdir %(pwd)" >> ~/.debmacros
