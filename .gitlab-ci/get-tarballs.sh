#!/bin/bash
# Script to run tito to get the correct tarballs
# Author: Neal Gompa <ngompa@datto.com>
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later

# Copyright (C) 2018  Datto, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


set -eu -o pipefail

# Check if rpmspec is installed
if [ ! -x /usr/bin/rpmspec ]; then
	dnf --assumeyes install /usr/bin/rpmspec
fi

# Check if spectool is installed
if [ ! -x /usr/bin/spectool ]; then
	dnf --assumeyes install /usr/bin/spectool
fi

# Check if tito is installed
if [ ! -x /usr/bin/tito ]; then
	dnf --assumeyes install /usr/bin/tito
fi

# Get versions of tito-managed packages
APT_SPACEWALK_VER=$(rpmspec -q --srpm --qf '%{version}\n' apt-spacewalk/apt-spacewalk.spec)
RHN_CLIENT_TOOLS_VER=$(rpmspec -q --srpm --qf '%{version}\n' rhn-client-tools/rhn-client-tools.spec)
RHNLIB_VER=$(rpmspec -q --srpm --qf '%{version}\n' rhnlib/rhnlib.spec)
RHNSD_VER=$(rpmspec -q --srpm --qf '%{version}\n' rhnsd/rhnsd.spec)
RHNCFG_VER=$(rpmspec -q --srpm --qf '%{version}\n' rhncfg/rhncfg.spec)
SPACEWALK_USIX_VER=$(rpmspec -q --srpm --qf '%{version}\n' spacewalk-usix/spacewalk-usix.spec)

BASEDIR=$(pwd)

# Checkout the spacewalk code
if [ ! -d /tmp/spacewalk ]; then
	git clone https://github.com/spacewalkproject/spacewalk.git /tmp/spacewalk
fi
cd /tmp/spacewalk

# Generate tarballs

## apt-spacewalk
git checkout apt-spacewalk-${APT_SPACEWALK_VER}-1
pushd client/debian/packages-already-in-debian/apt-transport-spacewalk
tito build --output=${BASEDIR}/apt-spacewalk --tgz
popd

## rhn-client-tools
git checkout rhn-client-tools-${RHN_CLIENT_TOOLS_VER}-1
pushd client/rhel/rhn-client-tools
tito build --output=${BASEDIR}/rhn-client-tools --tgz
popd

## rhnlib
git checkout rhnlib-${RHNLIB_VER}-1
pushd client/rhel/rhnlib
tito build --output=${BASEDIR}/rhnlib --tgz
popd

## rhnsd
git checkout rhnsd-${RHNSD_VER}-1
pushd client/rhel/rhnsd
tito build --output=${BASEDIR}/rhnsd --tgz
popd

## rhncfg
git checkout rhncfg-${RHNCFG_VER}-1
pushd client/tools/rhncfg
tito build --output=${BASEDIR}/rhncfg --tgz
popd

## spacewalk-usix
git checkout spacewalk-usix-${SPACEWALK_USIX_VER}-1
pushd usix
tito build --output=${BASEDIR}/spacewalk-usix --tgz
popd

# Change back to base directory
cd $BASEDIR

## python-hwdata
pushd python-hwdata
spectool -g python-hwdata.spec
popd

## python-dmidecode
pushd python-dmidecode
spectool -g python-dmidecode.spec
popd

